# Chuck Norris Joke Chatbot
This project is the backend for a Facebook Messenger chatbot that tells you jokes on-demand.
Say 'hello' to [ChuckieBot on Facebook Messenger](m.me/ChuckieBot) to try it.

This project utilizes the Nix package manager and NixOS, a Linux distribution built around Nix.

## Running
1. [Have Nix installed.](https://nixos.org/nix/download.html)
2. Have PostgreSQL running and available on localhost via UDS.
3. `nix-shell -A shell --pure --run app.js`

## Deployment
A self contained deployment utility is included in the `infrastructure` directory. It can deploy the chatbot to a DigitalOcean server, and continually update it afterwards.

To deploy the chatbot, do the following steps on a development machine.
1. [Have Nix installed.](https://nixos.org/nix/download.html)
2. `cd infrastructure`
3. Create the file `terraform.tfvars` with these contents and fill in the sensitive variables:

    ```
    domain = "<your domain>"
    do_token = "<your DigitalOcean token>"
    facebook_app_secret = "<your Facebook app secret>"
    facebook_page_access_token = "<your Facebook page access token>"
    facebook_validation_token = "<your Facebook Messenger validation token>"
    ```
4. `nix-shell --pure --run 'terraform apply'`

## License
See the LICENSE file.

This project is based on [Facebook Messenger Test Drive](https://developers.facebook.com/docs/messenger-platform/getting-started/test-drive/?testdrive_messengerbot=true).
