# nix-shell environment for provisioning infrastructure and deploymen

{pkgs ? import ../pinned-nixpkgs.nix {}, ...} :
pkgs.stdenv.mkDerivation {
  name = "chuckiebot-deploy-env";
  buildInputs = [ pkgs.terraform pkgs.git pkgs.rsync pkgs.bash ];
}
