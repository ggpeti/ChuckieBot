# NixOS module defining ChuckieBot server

{ config, lib, pkgs, ... } :
let
  cfg = config.services.chuckiebot;
  backend = (import ../default.nix {}).package;
in {
  options.services.chuckiebot = {
    domain = lib.mkOption {
      type = lib.types.string;
    };
    debug = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };

  config = {
    networking.firewall.allowedTCPPorts = [ 22 80 443 ];

    environment.systemPackages = [ pkgs.rsync ];

    users.extraUsers.chuckiebot = { group = "chuckiebot"; };
    users.extraGroups.chuckiebot = {};

    systemd.services.chuckiebot = {
      description = "chuckiebot backend";
      serviceConfig = {
        EnvironmentFile = "/root/secrets";
        User = "chuckiebot";
      };
      wants = [ "network.target" "chuckiebot-create-db.service" ];
      after = [ "network.target" "chuckiebot-create-db.service" ];
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      script = ''
        ${backend}/lib/node_modules/ChuckieBot/app.js
      '';
    };

    systemd.services.chuckiebot-create-db = {
      serviceConfig = {
        Type = "oneshot";
        User = "postgres";
      };
      wants = [ "postgresql.service" ];
      after = [ "postgresql.service" ];
      restartIfChanged = true;
      script = ''
        ${config.services.postgresql.package}/bin/createuser chuckiebot | true
        ${config.services.postgresql.package}/bin/createdb chuckiebot -O chuckiebot | true
      '';
    };

    services.postgresql = {
      enable = true;
      dataDir = "/data/postgresql";
    };

    services.nginx = {
      enable = true;
      recommendedOptimisation = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      virtualHosts."chuckiebot.${cfg.domain}" = {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:5000";
        };
      };
    };
  };
}
