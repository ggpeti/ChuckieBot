# Infrastructure definition

variable "do_token" {
  description = "Digital Ocean access token. See https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2#how-to-generate-a-personal-access-token"
}

variable "domain" {
  description = "The name of an existing Domain in your DigitalOcean account. Set it up here: https://cloud.digitalocean.com/networking/domains/"
}

variable "facebook_app_secret" {
  description = "App secret used by ChuckieBot to connect to Facebook."
}

variable "facebook_validation_token" {
  description = "Validation token used by ChuckieBot to connect to Facebook."
}

variable "facebook_page_access_token" {
  description = "Page access token used by ChuckieBot to connect to Facebook."
}

variable "do_region" {
  default = "nyc3"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "tls_private_key" "chuckiebot" {
  algorithm = "RSA"
}

resource "local_file" "private_key" {
  filename = "${path.module}/private_key"
  content  = "${tls_private_key.chuckiebot.private_key_pem}"

  provisioner "local-exec" {
    command = "chmod 600 ${path.module}/private_key"
  }
}

resource "digitalocean_ssh_key" "chuckiebot" {
  name       = "chuckiebot"
  public_key = "${tls_private_key.chuckiebot.public_key_openssh}"
}

data "template_file" "host-nixos-config" {
  template = <<EOF
# NixOS module for chuckiebot host system
# Provisioned by Terraform

let
  chuckiebotModulePath = "/root/chuckiebot/infrastructure/chuckiebot.nix";
in
  { config, pkgs, lib, ... } :
    if lib.pathExists chuckiebotModulePath then
      {
        imports = [ chuckiebotModulePath ];

        services.chuckiebot.domain = "${var.domain}";
        # services.chuckiebot.debug = true;

        system.autoUpgrade.enable = true;
      }
    else
      lib.warn ("ChuckieBot NixOS module not found at " + chuckiebotModulePath + " . Skipping ChuckieBot configuration.") {}
EOF
}

resource "digitalocean_droplet" "chuckiebot" {
  name       = "chuckiebot"
  size       = "2gb"
  image      = "debian-9-x64"
  region     = "${var.do_region}"
  ssh_keys   = ["${digitalocean_ssh_key.chuckiebot.id}"]
  ipv6       = true

  private_networking = true

  connection {
    user        = "root"
    host        = "${digitalocean_droplet.chuckiebot.ipv4_address}"
    private_key = "${tls_private_key.chuckiebot.private_key_pem}"
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "remote-exec" {
    inline = [
      "echo -e 'silent\nshow-error\nretry=2' | tee ~/.curlrc > /dev/null",
      "wget 'https://raw.githubusercontent.com/elitak/nixos-infect/769dce60c939add8fae5bc9f03fc96d8661a76a7/nixos-infect' -O /root/nixos-infect",
      "echo 'Installing NixOS. Logging to /tmp/infect.log.'",
      "NIXOS_IMPORT=/root/host.nix NIX_CHANNEL=nixos-17.09 bash /root/nixos-infect 2>&1 > /tmp/infect.log",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'NixOS version:'",
      "nixos-version",
      "echo 'Removing old system...'",
      "cp /old-root/root/host.nix /root/host.nix",
      "cp /old-root/root/.curlrc /root/.curlrc",
      "rm -rf /old-root",
      "echo Done."
    ]
  }
}

resource "digitalocean_floating_ip" "chuckiebot" {
  droplet_id = "${digitalocean_droplet.chuckiebot.id}"
  region     = "${digitalocean_droplet.chuckiebot.region}"
}

resource "digitalocean_record" "chuckiebot" {
  domain = "${var.domain}"
  type   = "A"
  name   = "chuckiebot"
  value  = "${digitalocean_floating_ip.chuckiebot.ip_address}"
}


resource "null_resource" "deploy" {
  depends_on = [ "digitalocean_record.chuckiebot" ]

  triggers {
    droplet = "${digitalocean_droplet.chuckiebot.id}"
    always  = "${uuid()}"
  }

  connection {
    user        = "root"
    host        = "${digitalocean_floating_ip.chuckiebot.ip_address}"
    private_key = "${tls_private_key.chuckiebot.private_key_pem}"
  }

  provisioner "remote-exec" {
    inline = ["echo 'Hello, World!'"]
  }

  provisioner "file" {
    content     = "${data.template_file.host-nixos-config.rendered}"
    destination = "/root/host.nix"
  }

  provisioner "file" {
    content     = <<EOF
MESSENGER_APP_SECRET=${var.facebook_app_secret}
MESSENGER_VALIDATION_TOKEN=${var.facebook_validation_token}
MESSENGER_PAGE_ACCESS_TOKEN=${var.facebook_page_access_token}
EOF
    destination = "/root/secrets"
  }

  provisioner "local-exec" {
    command = "./deploy -i ${path.module}/private_key root@${digitalocean_floating_ip.chuckiebot.ip_address}"
  }
}

output "ip" {
  value = "${digitalocean_floating_ip.chuckiebot.ip_address}"
}

output "fqdn" {
  value = "${digitalocean_record.chuckiebot.fqdn}"
}

output "private_key_file" {
  value = "${path.module}/private_key"
}
