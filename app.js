#!/usr/bin/env node

'use strict';

const
  bodyParser = require('body-parser'),
  express = require('express'),
  https = require('https'),
  request = require('request'),
  decode = require('decode-html'),
  fb = require('./fb');

const {
  Client
} = require('pg'),
  pgClient = new Client({
    host: "/tmp"
  });

pgClient.connect().then(() =>
  pgClient.query(
    `
    CREATE TABLE IF NOT EXISTS jokes_sent(
      id serial primary key,
      user_id text,
      ts timestamptz
    )
    `
  )
).then(() =>
  pgClient.query(
    "CREATE INDEX IF NOT EXISTS jokes_sent_ts_desc_index ON jokes_sent (user_id, ts DESC)"
  )
);

var app = express();
app.set('port', process.env.PORT || 5000);
app.use(bodyParser.json({
  verify: fb.verifyRequestSignature
}));
app.use(express.static('public'));

// App Secret can be retrieved from the App Dashboard
global.APP_SECRET = process.env.MESSENGER_APP_SECRET;

// Arbitrary value used to validate a webhook
global.VALIDATION_TOKEN = process.env.MESSENGER_VALIDATION_TOKEN;

// Generate a page access token for your page from the App Dashboard
global.PAGE_ACCESS_TOKEN = process.env.MESSENGER_PAGE_ACCESS_TOKEN

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
app.get('/webhook', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
    req.query['hub.verify_token'] === global.VALIDATION_TOKEN
  ) {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error(
      "Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);
  }
});


/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page.
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
app.post('/webhook', function(req, res) {
  var data = req.body;

  // Make sure this is a page subscription
  if (data.object == 'page') {
    // Iterate over each entry
    // There may be multiple if batched
    data.entry.forEach(function(pageEntry) {
      var pageID = pageEntry.id;
      var timeOfEvent = pageEntry.time;

      if (pageEntry.messaging) {

        // Iterate over each messaging event
        pageEntry.messaging.forEach(function(messagingEvent) {
          if (messagingEvent.message) {
            receivedMessage(messagingEvent);
          } else {
            console.log("Webhook received unknown messagingEvent: ",
              messagingEvent);
          }
        });
      } else {
        "Webhook received unknown event: ", pageEntry
      }
    });

    // Assume all went well.
    //
    // You must send back a 200, within 20 seconds, to let us know you've
    // successfully received the callback. Otherwise, the request will time out.
    res.sendStatus(200);
  }
});

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
function receivedMessage(event) {
  var senderID = event.sender.id;
  var recipientID = event.recipient.id;
  var timeOfMessage = event.timestamp;
  var message = event.message;

  console.log("Received message for user %d and page %d at %d with message:",
    senderID, recipientID, timeOfMessage);
  console.log(JSON.stringify(message));

  // You may get a text or attachment but not both
  var messageText = message.text;
  var messageAttachments = message.attachments;

  if (messageText) {

    // If we receive a text message, check to see if it matches any special
    // keywords and send back the corresponding example. Otherwise, just echo
    // the text we received.
    switch (messageText.replace(/[^\w\s]/gi, '').trim().toLowerCase()) {
      case 'joke':
        limitedSend(senderID);
        break;

      case 'more':
        sendMore(senderID);
        break;

      case 'reset':
        deleteRecords(senderID)
        fb.sendTextMessage(senderID, "You may now request more jokes.")
        break;

      case 'hello':
        fb.sendTextMessage(senderID,
          `I understand these words:
- joke: I will send you a joke.
- more: I will send you another joke.
- reset: Reset your record in order to receive even more jokes.
- hello: Display this text.`
        );
        break;

      default:
        fb.sendTextMessage(senderID, messageText);
    }
  }
}

function limitedSend(recipientId) {
  pgClient.query(
    "SELECT COUNT(*) AS count FROM jokes_sent WHERE user_id = $1 AND ts > current_timestamp - interval '1 day'", [
      recipientId
    ], (
      err, res) => {
      if (res.rows[0].count >= 10) {
        fb.sendTextMessage(recipientId,
          "Too much laughter will kill you! Only 10 jokes every 24 hours."
        );
      } else {
        sendJoke(recipientId);
      }
    }
  )
}

function sendJoke(recipientId) {
  noteTimestamp(recipientId);
  https.get('https://api.icndb.com/jokes/random', (res) => {
    const {
      statusCode
    } = res;
    const contentType = res.headers['content-type'];

    let error;
    if (statusCode !== 200) {
      error = new Error('Request Failed.\n' +
        `Status Code: ${statusCode}`);
    } else if (!/^application\/json/.test(contentType)) {
      error = new Error('Invalid content-type.\n' +
        `Expected application/json but received ${contentType}`);
    }
    if (error) {
      console.error(error.message);
      // consume response data to free up memory
      res.resume();
      return;
    }
    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => {
      rawData += chunk;
    });
    res.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData);
        fb.sendTextMessage(recipientId, decode(parsedData.value.joke));
      } catch (e) {
        console.error(e.message);
      }
    });
  }).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
  });
}

function noteTimestamp(recipientId) {
  pgClient.query('INSERT INTO jokes_sent (user_id, ts) VALUES ($1, $2)', [
    recipientId, new Date
  ]);
}

function sendMore(recipientId) {
  pgClient.query(
    "SELECT COUNT(*) AS count FROM jokes_sent WHERE user_id = $1 LIMIT 1", [
      recipientId
    ], (
      err, res) => {
      if (res.rows[0].count == 0) {
        fb.sendTextMessage(recipientId,
          "More of what? (Hint: ask for a joke first.)"
        );
      } else {
        limitedSend(recipientId);
      }
    }
  )
}

function deleteRecords(recipientId) {
  pgClient.query("DELETE FROM jokes_sent WHERE user_id = $1", [recipientId])
}

// Start server
// Webhooks must be available via SSL with a certificate signed by a valid
// certificate authority.
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

module.exports = app;
