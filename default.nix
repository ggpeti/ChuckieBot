{ pkgs ? import ./pinned-nixpkgs.nix {}, ... }:
import ./node-composite.nix {
  inherit pkgs;
}
